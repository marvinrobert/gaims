const express = require("express");
const router = express.Router();
const nodemailer = require("nodemailer");
const bodyParser = require("body-parser");

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
    extended: true
}));

router.post('/sendmessage', (req, res) => {
    var formdata = " Message from " + req.body.contactName +
        "\n Email: <" + req.body.contactEmail +
        "> \n Message: " + req.body.contactMessage;

    var transporter = nodemailer.createTransport({
        service: 'gmail',
        port: 465,
        auth: {
            user: 'bigdataghanainfo@gmail.com',
            pass: 'b1gd4t4ghana'
        }
    });

    var mailOptions = {
        from: req.body.contactName + " <" + formdata.contactEmail + ">",
        to: 'marvin.mohenu@bigdataghana.com',
        subject: 'Contact Form - Message',
        text: formdata
    };

    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
            res.redirect("/contact?error=true");
        } else {
            var responseToSender = {
                from: "GAIMS - BigData Ghana Ltd. <info@bigdataghana.com>",
                to: req.body.contactEmail,
                subject: 'NoReply: GAIMS - BigData Ghana Ltd Response',
                text: "Hello " + req.body.contactName +
                    ",\n Your messsage was received successfully. A member of the GAIMS development team will get in touch with you." +
                    "\n Thank you." +
                    "\n\n Regards," +
                    "\n\n BigDataGhana Ltd - GAIMS Developer"
            };
            transporter.sendMail(responseToSender, function(error, info){
                if (error){
                    console.log(error);
                } else {
                    console.log("Response Email sent to Sender");
                }
            });
            console.log('Email sent: ' + info.response);
            res.redirect("/contact?success=true");
        }
    });
});

router.post('/senddataset', (req, res) => {
    var formdata = " Dataset suggested by " + req.body.suggestName +
        "\n Email: <" + req.body.suggestEmail +
        "> \n Dataset: " + req.body.suggestDataset +
        "\n Purpose: " + req.body.suggestPurpose;

    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'bigdataghanainfo@gmail.com',
            pass: 'b1gd4t4ghana'
        }
    });

    var mailOptions = {
        // from: 'GAIMS Website - BigData Ghana Ltd <bigdataghanainfo@gmail.com>',
        from: req.body.suggestName + " <" + req.body.suggestEmail + ">",
        to: 'marvin.mohenu@bigdataghana.com',
        subject: 'Contact Form - Dataset Suggestion',
        text: formdata
    };

    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
            res.redirect("/contact?error=true");
        } else {
            var responseToSender = {
                from: "GAIMS - BigData Ghana Ltd. <info@bigdataghana.com>",
                to: req.body.suggestEmail,
                subject: 'NoReply: GAIMS - BigData Ghana Ltd Response',
                text: "Hello " + req.body.suggestName +
                    ",\n Your suggestion was received successfully." +
                    "\n The suggested dataset will be investigated and worked on. Do not hesitate to get in touch with us again." +
                    "\n Thank you." +
                    "\n\n Regards," +
                    "\n\n BigDataGhana Ltd - GAIMS Developer"
            };
            transporter.sendMail(responseToSender, function(error, info){
                if (error){
                    console.log(error);
                } else {
                    console.log("Response Email sent to Sender");
                }
            });
            console.log('Email sent: ' + info.response);
            res.redirect("/contact?success=true");
        }
    });
});

router.post('/requestaccess', (req, res) => {
    var formdata = " Request from " + req.body.requestName +
        "\n Email: <" + req.body.requestEmail +
        "> \n Data requested for: " + req.body.requestData +
        "\n Category: " + req.body.requestCategory +
        "> \n Section: " + req.body.requestSection +
        "\n Format: " + req.body.requestFormat;

    var transporter = nodemailer.createTransport({
        service: 'gmail',
        port: 465,
        auth: {
            user: 'bigdataghanainfo@gmail.com',
            pass: 'b1gd4t4ghana'
        }
    });

    var mailOptions = {
        from: req.body.requestName + " <" + formdata.requestEmail + ">",
        to: 'marvin.mohenu@bigdataghana.com',
        subject: 'Request for Access to Data',
        text: formdata
    };

    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
            res.redirect("/open-data?error=true");
        } else {
            var responseToSender = {
                from: "GAIMS - BigData Ghana Ltd. <info@bigdataghana.com>",
                to: req.body.requestEmail,
                subject: 'NoReply: GAIMS - BigData Ghana Ltd Response',
                text: "Hello " + req.body.requestName +
                    ",\n Your request was received successfully. A member of the GAIMS development team will get in touch with you." +
                    "\n Thank you." +
                    "\n\n Regards," +
                    "\n\n BigDataGhana Ltd - GAIMS Developer"
            };
            transporter.sendMail(responseToSender, function(error, info){
                if (error){
                    console.log(error);
                } else {
                    console.log("Response Email sent to Sender");
                }
            });
            console.log('Email sent: ' + info.response);
            // res.redirect("/open-data?success=true");
            res.json("success");
        }
    });
});

module.exports = router;