const express = require('express');
const exphbs = require('express-handlebars');
const path = require('path');

const app = express();

// Import route
const users = require('./routes/users');
const formroute = require('./config/form-mailer');

// MIDDLEWARE

// Handlebars Middleware
app.engine('handlebars', exphbs({
    defaultLayout: 'main'
}));
app.set('view engine', 'handlebars');

// Static folder
app.use(express.static(path.join(__dirname, 'public')));


// ROUTES

// Index route
app.get('/', (req, res) => {
    res.render('home');
});

// Open data route
app.get('/open-data', (req, res) => {
    res.render('open-data');
});

// Gaims Premium route
app.get('/gaims-premium', (req, res) => {
    res.render('gaims-premium');
});

// Events route
app.get('/events', (req, res) => {
    res.render('events');
});

// News route
app.get('/news', (req, res) => {
    res.render('news');
});

// More route
app.get('/contact', (req, res) => {
    res.render('contact');
});

// Users route
app.use('/users', users);

// Send Mesage route
app.use('/form', formroute);

const port = 3000;

app.listen(port, () => {
   console.log(`Server started on port ${port}`);
});