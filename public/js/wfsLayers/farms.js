if (window.location.href.indexOf("/open-data") > -1) {
    var owsrootUrl = 'https://geoserver.bigdataghana.com/geoserver/ows';

    var farmsLayer = null;

    var farmsParameters = {
        service: 'WFS',
        version: '2.0',
        request: 'GetFeature',
        typeName: 'gaims:gaims_farms',
        outputFormat: 'text/javascript',
        format_options: 'callback:getJson',
        SrsName: 'EPSG:4326'
    };

    var farmsExtendparameters = L.Util.extend(farmsParameters);
    var farmsURL = owsrootUrl + L.Util.getParamString(farmsExtendparameters);

    function farmsAjax() {
        map.spin(true, { corners: 0.1, lines: 11, width: 8, radius: 17, scale: 1.05, length: 0 });
        $.ajax({
            url: farmsURL,
            dataType: 'jsonp',
            jsonpCallback: 'getJson',
            success: function(response) {
                farmsLayer = L.geoJson(response, {
                    style: farmCategory,
                    onEachFeature: onEachFarmFeature,
                    pane: "farmsPane",
                }).addTo(map);
                map.spin(false);
                $('.maizeFarmLegend').removeClass('hide-legend');
                $('.cashewFarmLegend').removeClass('hide-legend');
                $('.riceFarmLegend').removeClass('hide-legend');
                $('.soyaBeanFarmLegend').removeClass('hide-legend');
                $('.otherCropFarmLegend').removeClass('hide-legend');
            }
        });
    }

    function addFarmsLayer() {
        farmsAjax();
    }

    function removeFarmsLayer() {
        farmsLayer.removeFrom(map);
        $('.maizeFarmLegend').addClass('hide-legend');
        $('.cashewFarmLegend').addClass('hide-legend');
        $('.riceFarmLegend').addClass('hide-legend');
        $('.soyaBeanFarmLegend').addClass('hide-legend');
        $('.otherCropFarmLegend').addClass('hide-legend');
    }

    // Highlight region on mouse hover
    function highlightFarmFeature(e) {
        var layer = e.target;
        layer.setStyle({
            fillOpacity: 0,
            stroke: true,
            fillColor: '#FFFFFF',
            fillOpacity: 0
        });
        if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
            layer.bringToFront();
        }
        var hovered = layer.feature.properties.crop;
        layer.bindTooltip("<strong>Crop: </strong>" + hovered).openTooltip();

        // layer.bindTooltip("<strong>Region: </strong>" + layer.feature.properties.region +
        //     "<br><strong>District: </strong>" + layer.feature.properties.district +
        //     "<br><strong>Size: </strong>" + hovered + " acres" +
        //     "<br><strong>Date Recorded: </strong>" + layer.feature.properties.date).openTooltip();
    }

    function resetFarmHighlight(e) {
        farmsLayer.resetStyle(e.target);
    }

    function zoomToFarm(e) {
        map.flyToBounds(e.target.getBounds());
        var layer = e.target;
        layer.setStyle({
            color: "#000000",
            weight: 2,
        });
    }

    function onEachFarmFeature(feature, layer) {
        layer.on({
            mouseover: highlightFarmFeature,
            mouseout: resetFarmHighlight,
            click: openFarmPopUp,
            // dblclick: openFarmPopUp,
        });
    }

    function farmCategory(feature) {
        switch (feature.properties.crop) {
            // case "Okro":
            //     return {
            //         stroke: true,
            //         color: '#800000',
            //         fillColor: '#800000',
            //         fillOpacity: 1
            //     };
            //     break;
            case "Cashew":
                return {
                    stroke: true,
                    color: '#2980b9',
                    fillColor: '#2980b9',
                    fillOpacity: 1
                };
                break;
                // case "Shea nut":
                //     return {
                //         stroke: true,
                //         color: '#9A6324',
                //         fillColor: '#9A6324',
                //         fillOpacity: 1
                //     };
                //     break;
                // case "Yam":
                //     return {
                //         stroke: true,
                //         color: '#f58231',
                //         fillColor: '#f58231',
                //         fillOpacity: 1
                //     };
                //     break;
                // case "Pepper":
                //     return {
                //         stroke: true,
                //         color: '#469990',
                //         fillColor: '#469990',
                //         fillOpacity: 1
                //     };
                //     break;
                // case "Sorghum":
                //     return {
                //         stroke: true,
                //         color: '#000075',
                //         fillColor: '#000075',
                //         fillOpacity: 1
                //     };
                //     break;
                // case "Mango":
                //     return {
                //         stroke: true,
                //         color: '#42d4f4',
                //         fillColor: '#42d4f4',
                //         fillOpacity: 1
                //     };
                //     break;
            case "Maize":
                return {
                    stroke: true,
                    color: '#e74c3c',
                    fillColor: '#e74c3c',
                    fillOpacity: 1
                };
                break;
                // case "Cassava":
                //     return {
                //         stroke: true,
                //         color: '#911eb4',
                //         fillColor: '#911eb4',
                //         fillOpacity: 1
                //     };
                //     break;
                // case "Groundnut":
                //     return {
                //         stroke: true,
                //         color: '#f032e6',
                //         fillColor: '#f032e6',
                //         fillOpacity: 1
                //     };
                //     break;
                // case "Beans":
                //     return {
                //         stroke: true,
                //         color: '#219ace',
                //         fillColor: '#219ace',
                //         fillOpacity: 1
                //     };
                //     break;
                // case "Millet":
                //     return {
                //         stroke: true,
                //         color: '#333333',
                //         fillColor: '#333333',
                //         fillOpacity: 1
                //     };
                //     break;
            case "Rice":
                return {
                    stroke: true,
                    color: '#008000',
                    fillColor: '#008000',
                    fillOpacity: 1
                };
                break;
            case "Soyabeans":
                return {
                    stroke: true,
                    color: '#a51cb3',
                    fillColor: '#a51cb3',
                    fillOpacity: 1
                };
                break;
                // case "Tomatoes":
                //     return {
                //         stroke: true,
                //         color: '#ff4500',
                //         fillColor: '#ff4500',
                //         fillOpacity: 1
                //     };
                //     break;
                // case "Cowpea":
                //     return {
                //         stroke: true,
                //         color: '#800080',
                //         fillColor: '#800080',
                //         fillOpacity: 1
                //     };
                //     break;
                // Other crops
            default:
                return {
                    stroke: true,
                    color: '#000080',
                    fillColor: '#000080',
                    fillOpacity: 1
                };
        }
        // if (feature.properties.crop == "Maize") {
        //     return {
        //         stroke: true,
        //         color: '#219ace',
        //         fillColor: '#219ace',
        //         fillOpacity: 1
        //     };
        // } else if (feature.properties.crop == "Cashew") {
        //     return {
        //         stroke: true,
        //         color: '#a10011',
        //         fillColor: '#a10011',
        //         fillOpacity: 1
        //     };
        // } else if (feature.properties.crop == "Rice") {
        //     return {
        //         stroke: true,
        //         color: '#afcf26',
        //         fillColor: '#afcf26',
        //         fillOpacity: 1
        //     };
        // }
    }

    function openFarmPopUp(e) {
        var layer = e.target;
        zoomToFarm(e);

        popupOptions = { maxWidth: 200, maxHeight: 400 };
        var wfsLayerProperties = "<h6 style='background-color: #0c396d; color: white; border-radius: 5px;' class='text-center py-2'><i class='fas fa-seedling'></i> " + layer.feature.properties.crop + " Farm</h6>" +
            "<table class=\"table table-bordered\">\n" +
            "<tbody>" +
            "<tr class='my-0 py-0'><th class='py-1'>Region: </th><td class='py-1'>" + layer.feature.properties.region + "</td></p>" +
            "<tr class='my-0 py-0'><th class='py-1'>District: </th><td class='py-1'>" + layer.feature.properties.district + "</td></tr>" +
            "<tr class='my-0 py-0'><th class='py-1'>Size: </th><td class='py-1'>" + layer.feature.properties.acres + "</td></tr>" +
            "<tr class='my-0 py-0'><th class='py-1'>Date: </th><td class='py-1'>" + layer.feature.properties.date + "</td></tr>" +
            "</tbody>\n" +
            "</table>";

        layer.bindPopup(wfsLayerProperties, popupOptions);
    }
}