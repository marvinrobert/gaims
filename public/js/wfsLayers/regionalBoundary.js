if (window.location.href.indexOf("/open-data") > -1) {
    var owsrootUrl = 'https://geoserver.bigdataghana.com/geoserver/ows';

    var regionalBoundaryLayer = null;

    var regionalBoundaryParameters = {
        service: 'WFS',
        version: '2.0',
        request: 'GetFeature',
        typeName: 'gaims:regional_boundary_16',
        outputFormat: 'text/javascript',
        format_options: 'callback:getJson',
        SrsName: 'EPSG:4326'
    };

    var regionalBoundaryExtendparameters = L.Util.extend(regionalBoundaryParameters);
    var regionalBoundaryURL = owsrootUrl + L.Util.getParamString(regionalBoundaryExtendparameters);

    function regionalBoundaryAjax() {
        map.spin(true, { corners: 0.1, lines: 11, width: 8, radius: 17, scale: 1.05, length: 0 });
        $.ajax({
            url: regionalBoundaryURL,
            dataType: 'jsonp',
            jsonpCallback: 'getJson',
            success: function(response) {
                regionalBoundaryLayer = L.geoJson(response, {
                    style: function(feature) {
                        return {
                            stroke: true,
                            color: '#2a2a2a',
                            fillColor: '#646464',
                            fillOpacity: 0.3
                        };
                    },
                    pane: "regionalPane",
                    onEachFeature: onEachRegionFeature,
                }).addTo(map);
                map.spin(false);
                $('.regionalBoundaryLegend').removeClass('hide-legend');
            },
        });
    }

    function addRegionalBoundaryLayer() {
        regionalBoundaryAjax();
    }

    function removeRegionalBoundaryLayer() {
        regionalBoundaryLayer.removeFrom(map);
        $('.regionalBoundaryLegend').addClass('hide-legend');
    }

    // Highlight region on mouse hover
    function highlightRegionFeature(e) {
        var layer = e.target;
        layer.setStyle({
            fillOpacity: 0,
            stroke: true,
            color: '#161616',
            fillColor: '#FFFFFF',
            fillOpacity: 0
        });
        if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
            layer.bringToFront();
        }
        var hovered = layer.feature.properties.region;
        layer.bindTooltip(`<strong>Region: </strong>` + hovered).openTooltip();
    }

    function resetRegionHighlight(e) {
        regionalBoundaryLayer.resetStyle(e.target);
    }

    function zoomToRegion(e) {
        map.flyToBounds(e.target.getBounds());
        var layer = e.target;
        layer.setStyle({
            color: "#000000",
            weight: 2,
        });
    }

    function onEachRegionFeature(feature, layer) {
        layer.on({
            mouseover: highlightRegionFeature,
            mouseout: resetRegionHighlight,
            click: zoomToRegion
        });
    }
}