if (window.location.href.indexOf("/open-data") > -1) {
    var owsrootUrl = 'https://geoserver.bigdataghana.com/geoserver/ows';

    var constituencyBoundaryLayer = null;

    var constituencyBoundaryParameters = {
        service: 'WFS',
        version: '2.0',
        request: 'GetFeature',
        typeName: 'gaims:constituency_boundary',
        outputFormat: 'text/javascript',
        format_options: 'callback:getJson',
        SrsName: 'EPSG:4326'
    };

    var constituencyBoundaryExtendparameters = L.Util.extend(constituencyBoundaryParameters);
    var constituencyBoundaryURL = owsrootUrl + L.Util.getParamString(constituencyBoundaryExtendparameters);

    function constituencyBoundaryAjax() {
        map.spin(true, { corners: 0.1, lines: 11, width: 8, radius: 17, scale: 1.05, length: 0 });
        $.ajax({
            url: constituencyBoundaryURL,
            dataType: 'jsonp',
            jsonpCallback: 'getJson',
            success: function(response) {
                constituencyBoundaryLayer = L.geoJson(response, {
                    style: function(feature) {
                        return {
                            stroke: true,
                            color: '#800000',
                            fillColor: '#646464',
                            fillOpacity: 0.3
                        };
                    },
                    pane: "districtPane",
                    onEachFeature: onEachConstituencyFeature,
                }).addTo(map);
                map.spin(false);
                $('.constituencyBoundaryLegend').removeClass('hide-legend');
            }
        });
    }

    function addConstituencyBoundaryLayer() {
        constituencyBoundaryAjax();
    }

    function removeConstituencyBoundaryLayer() {
        constituencyBoundaryLayer.removeFrom(map);
        $('.constituencyBoundaryLegend').addClass('hide-legend');
    }

    // Highlight region on mouse hover
    function highlightConstituencyFeature(e) {
        var layer = e.target;
        layer.setStyle({
            fillOpacity: 0,
            stroke: true,
            color: '#161616',
            fillColor: '#FFFFFF',
            fillOpacity: 0
        });
        if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
            layer.bringToFront();
        }
        var hovered = layer.feature.properties.constituen;
        layer.bindTooltip(`<strong>Constituency: </strong>` + hovered).openTooltip();
    }

    function resetConstituencyHighlight(e) {
        constituencyBoundaryLayer.resetStyle(e.target);
    }

    function zoomToConstituency(e) {
        map.flyToBounds(e.target.getBounds());
        var layer = e.target;
        layer.setStyle({
            color: "#000000",
            weight: 2,
        });
    }

    function onEachConstituencyFeature(feature, layer) {
        layer.on({
            mouseover: highlightConstituencyFeature,
            mouseout: resetConstituencyHighlight,
            click: zoomToConstituency
        });
    }
}