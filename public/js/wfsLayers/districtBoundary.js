if (window.location.href.indexOf("/open-data") > -1) {
    var owsrootUrl = 'https://geoserver.bigdataghana.com/geoserver/ows';

    var districtBoundaryLayer = null;

    var districtBoundaryParameters = {
        service: 'WFS',
        version: '2.0',
        request: 'GetFeature',
        typeName: 'gaims:district_boundary',
        outputFormat: 'text/javascript',
        format_options: 'callback:getJson',
        SrsName: 'EPSG:4326'
    };

    var districtBoundaryExtendparameters = L.Util.extend(districtBoundaryParameters);
    var districtBoundaryURL = owsrootUrl + L.Util.getParamString(districtBoundaryExtendparameters);

    function districtBoundaryAjax() {
        map.spin(true, { corners: 0.1, lines: 11, width: 8, radius: 17, scale: 1.05, length: 0 });
        $.ajax({
            url: districtBoundaryURL,
            dataType: 'jsonp',
            jsonpCallback: 'getJson',
            success: function(response) {
                districtBoundaryLayer = L.geoJson(response, {
                    style: function(feature) {
                        return {
                            stroke: true,
                            color: '#006400',
                            fillColor: '#646464',
                            fillOpacity: 0.3
                        };
                    },
                    pane: "districtPane",
                    onEachFeature: onEachDistrictFeature,
                }).addTo(map);
                map.spin(false);
                $('.districtBoundaryLegend').removeClass('hide-legend');
            }
        });
    }

    function addDistrictBoundaryLayer() {
        districtBoundaryAjax();
    }

    function removeDistrictBoundaryLayer() {
        districtBoundaryLayer.removeFrom(map);
        $('.districtBoundaryLegend').addClass('hide-legend');
    }

    // Highlight region on mouse hover
    function highlightDistrictFeature(e) {
        var layer = e.target;
        layer.setStyle({
            fillOpacity: 0,
            stroke: true,
            color: '#161616',
            fillColor: '#FFFFFF',
            fillOpacity: 0
        });
        if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
            layer.bringToFront();
        }
        const districtName = layer.feature.properties.district;
        layer.bindTooltip(`<strong>District: </strong>` + districtName).openTooltip();
    }

    function resetDistrictHighlight(e) {
        districtBoundaryLayer.resetStyle(e.target);
    }

    function zoomToDistrict(e) {
        map.flyToBounds(e.target.getBounds());
        var layer = e.target;
        layer.setStyle({
            color: "#000000",
            weight: 2,
        });
    }

    function onEachDistrictFeature(feature, layer) {
        layer.on({
            mouseover: highlightDistrictFeature,
            mouseout: resetDistrictHighlight,
            click: zoomToDistrict
        });
    }
}