if (window.location.href.indexOf("/open-data") > -1) {
    var owsrootUrl = 'https://geoserver.bigdataghana.com/geoserver/ows';

    var agroWarehousesLayer = null;

    var agroWarehousesParameters = {
        service: 'WFS',
        version: '2.0',
        request: 'GetFeature',
        typeName: 'gaims:tbl_agro_warehouses',
        outputFormat: 'text/javascript',
        format_options: 'callback:getJson',
        SrsName: 'EPSG:4326'
    };
    var agroWarehousesMarkerOptions = L.icon({
        iconUrl: '../../img/agrowarehouses-marker.png',
        iconSize: [50, 50],
        iconAnchor: [25, 50]
    });

    var agroWarehousesExtendparameters = L.Util.extend(agroWarehousesParameters);
    var agroWarehouseURL = owsrootUrl + L.Util.getParamString(agroWarehousesExtendparameters);

    function agroWarehousesAjax() {
        map.spin(true, { corners: 0.1, lines: 11, width: 8, radius: 17, scale: 1.05, length: 0 });
        $.ajax({
            url: agroWarehouseURL,
            dataType: 'jsonp',
            jsonpCallback: 'getJson',
            success: function(response) {
                agroWarehousesLayer = L.geoJson(response, {
                    pane: "agroWarehousesPane",
                    pointToLayer: function(feature, latlng) {
                        return L.marker(latlng, { icon: agroWarehousesMarkerOptions });
                    },
                    onEachFeature: onEachAgroWarehouseFeature,
                }).addTo(map);
                map.spin(false);
                $('.agroWarehousesLegend').removeClass('hide-legend');
            }
        });
    }

    function addAgroWarehousesLayer() {
        agroWarehousesAjax();
    }

    function removeAgroWarehousesLayer() {
        agroWarehousesLayer.removeFrom(map);
        $('.agroWarehousesLegend').addClass('hide-legend');
    }

    function highlightAgroWarehouseFeature(e) {
        const layer = e.target;

        // var hovered = ;
        layer.bindTooltip("<strong>Region: </strong>" + layer.feature.properties.region +
            "<br><strong>Type: </strong>" + layer.feature.properties.type +
            "<br><strong>Capacity: </strong>" + layer.feature.properties.capacity_1 + ' tonnes').openTooltip();
    }

    function openAgroWarehousePopUp(e) {
        var layer = e.target;

        popupOptions = { maxWidth: 200, maxHeight: 400 };
        var wfsLayerProperties = "<h6 style='background-color: #0c396d; color: white; border-radius: 5px;' class='text-center py-2'><i class='fas fa-warehouse'></i> Agro Warehouse</h6>" +
            "<table class=\"table table-bordered\">\n" +
            "<tbody>" +
            "<tr class='my-0 py-0'><th class='py-1'>Region: </th><td class='py-1'>" + layer.feature.properties.region + "</td></p>" +
            "<tr class='my-0 py-0'><th class='py-1'>District: </th><td class='py-1'>" + layer.feature.properties.district + "</td></tr>" +
            "<tr class='my-0 py-0'><th class='py-1'>Community: </th><td class='py-1'>" + layer.feature.properties.community + "</td></tr>" +
            "<tr class='my-0 py-0'><th class='py-1'>Type: </th><td class='py-1'>" + layer.feature.properties.type + "</td></tr>" +
            "<tr class='my-0 py-0'><th class='py-1'>Capacity: </th><td class='py-1'>" + layer.feature.properties.capacity_1 + " tonnes" + "</td></tr>" +
            "</tbody>\n" +
            "</table>";

        layer.bindPopup(wfsLayerProperties, popupOptions);
    }

    function onEachAgroWarehouseFeature(feature, layer) {
        layer.on({
            mouseover: highlightAgroWarehouseFeature,
            click: openAgroWarehousePopUp
        });
    }
}