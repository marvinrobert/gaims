if (window.location.href.indexOf("/open-data") > -1) {
    var digitalEarthWMSLayers = {
        // Annual Geometric Median NDVI
        annualGeometricMedianNDVI: L.tileLayer.wms('https://ows.digitalearth.africa/wms', {
            // layers: 'ga_ls8c_gm_2_annual',
            layers: 'ga_s2_gm',
            styles: 'ndvi',
            transparent: 'true',
            format: 'image/png',
            pane: "ndviPane",
            bounds: L.latLngBounds([11.1748836890268, -3.26206548438522], [4.73882972909699, 1.20019812188643])
        }).addTo(map),

        digitalElevationModelColors: L.tileLayer.wms('https://ows.digitalearth.africa/wms', {
            layers: 'srtm',
            styles: 'colours',
            transparent: 'true',
            format: 'image/png',
            pane: "demPane",
            bounds: L.latLngBounds([11.1748836890268, -3.26206548438522], [4.73882972909699, 1.20019812188643])
        }),
        digitalElevationModelGreyscale: L.tileLayer.wms('https://ows.digitalearth.africa/wms', {
            layers: 'srtm',
            styles: 'greyscale',
            transparent: 'true',
            format: 'image/png',
            pane: "demPane",
            bounds: L.latLngBounds([11.1748836890268, -3.26206548438522], [4.73882972909699, 1.20019812188643])
        }),
    };
    $('.annualGeometricMedianNDVI').removeClass('hide-legend');
}