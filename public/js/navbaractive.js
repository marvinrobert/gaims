$(document).ready(function() {
    if (window.location.href.indexOf("/open-data") > -1) {
        $("#whiteLogo").hide();
        $('#homenav').removeClass('active-nav');
        $('#opendatanav').addClass('active-nav');
        $('#footer').hide();
    } else {
        $('#opendatanav').removeClass('active-nav');
    }

    // if (window.location.href.indexOf("/maps") > -1) {
    //     $('#homenav').removeClass('active-nav');
    //     $('#mapsnav').addClass('active-nav');
    // } else {
    //     $('#mapsnav').removeClass('active-nav');
    // }

    if (window.location.href.indexOf("/gaims-premium") > -1) {
        $("#whiteLogo").hide();
        $('#homenav').removeClass('active-nav');
        $('#premiumnav').addClass('active-nav');
    } else {
        $('#premiumnav').removeClass('active-nav');
    }

    if (window.location.href.indexOf("/events") > -1) {
        $("#whiteLogo").hide();
        $('#homenav').removeClass('active-nav');
        $('#eventsnav').addClass('active-nav');
    } else {
        $('#eventsnav').removeClass('active-nav');
    }

    if (window.location.href.indexOf("/news") > -1) {
        $("#whiteLogo").hide();
        $('#homenav').removeClass('active-nav');
        $('#newsnav').addClass('active-nav');
    } else {
        $('#newsnav').removeClass('active-nav');
    }

    if (window.location.href.indexOf("/contact") > -1) {
        $("#whiteLogo").hide();
        $('#homenav').removeClass('active-nav');
        $('#contactnav').addClass('active-nav');
    } else {
        $('#contactnav').removeClass('active-nav');
    }
});