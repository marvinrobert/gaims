$(document).ready(function () {
    // Hide other pages on load
    $('.hidden-pages').hide();

    // Show page on click
    $('.page-link').click(function () {
        var page = $(this).html();

        $('.news-pages').hide();
        $('.page-link').removeClass('active-page');

        $(this).addClass('active-page');
        $('#page'+page).show();
    });
});