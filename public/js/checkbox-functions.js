if (window.location.href.indexOf("/open-data") > -1) {
    // Set checkboxes to checked
    $('#regionalBoundary').prop('checked', true);
    $('#annualGeometricMedianNDVI').prop('checked', true);
    $('#farms').prop('checked', true);

    // Checkbox functions
    $('input[type="checkbox"]').click(function() {
        // if ($(this).hasClass('bdgWMSLayer')) {
        //     if ($(this).is(':checked')) {
        //         bdgWMSLayers[$(this).val()].addTo(map);
        //     } else {
        //         bdgWMSLayers[$(this).val()].removeFrom(map);
        //     }
        // }

        if ($(this).is(':checked')) {
            if ($(this).val() == 'agroWarehouses') {
                addAgroWarehousesLayer();
            }

            if ($(this).val() == 'farms') {
                addFarmsLayer();
            }

            if ($(this).val() == 'regionalBoundary') {
                addRegionalBoundaryLayer();
            }

            if ($(this).val() == 'districtBoundary') {
                addDistrictBoundaryLayer();
            }

            if ($(this).val() == 'constituencyBoundary') {
                addConstituencyBoundaryLayer();
            }
        } else {
            if ($(this).val() == 'agroWarehouses') {
                removeAgroWarehousesLayer();
            }

            if ($(this).val() == 'farms') {
                removeFarmsLayer();
            }

            if ($(this).val() == 'regionalBoundary') {
                removeRegionalBoundaryLayer();
            }

            if ($(this).val() == 'districtBoundary') {
                removeDistrictBoundaryLayer();
            }

            if ($(this).val() == 'constituencyBoundary') {
                removeConstituencyBoundaryLayer();
            }
        }

        if ($(this).hasClass('digitalEarthWMSLayer')) {
            if ($(this).is(':checked')) {
                digitalEarthWMSLayers[$(this).val()].addTo(map);
                var tempVal = $(this).val();
                $("." + tempVal).removeClass('hide-legend');
            } else {
                digitalEarthWMSLayers[$(this).val()].removeFrom(map);
                var tempVal = $(this).val();
                $("." + tempVal).addClass('hide-legend');
            }
        }
    });
}