$(document).ready(function(){
    var scroll_start = 0;
    var target = $('#pageTitle');
    var offset = {top: 10, left:0};
    var clickCount = 0;
    if($("#homenav").hasClass("active-nav") == true) {
        $("#logo").attr('src', '/img/GAIMS-Logo-White.png');
        if($(".navbar-collapse").hasClass("show") == true) {
            console.log("showing");
        }
        else {
            $("#navbarToggler").click(function (){
                clickCount += 1;
                if(clickCount == 2){
                    clickCount = 0;
                    $("#navbar").css('background-color', 'transparent');
                    $(".nav-item").css('color', '#ffffff');
                    $("#navbar").removeClass('navbar-box-shadow');
                    $(".navbar-toggler").css('border', '2px solid #ffffff');
                    $(".navbar-toggler").css('color', '#ffffff');
                    $(".navbar-toggler-icon").css('color', '#ffffff');
                    $("#logo").attr('src', '/img/GAIMS-Logo-White.png');
                } else {
                    $("#navbar").css('background-color', '#ffffff');
                    $(".nav-item").css('color', '#002424');
                    $(".navbar-toggler").css('border', '2px solid #002424');
                    $(".navbar-toggler").css('color', '#002424');
                    $(".navbar-toggler-icon").css('color', '#002424');
                    $("#logo").attr('src', '/img/GAIMS-Logo-Green.png');
                }
            });
        }
        $("#navbar").css('background-color', 'transparent');
        $("#navbar").removeClass('navbar-box-shadow');
        $(".nav-item").css('color', '#ffffff');
        $(".active-nav").css('color', '#ffffff');
        $(".active-nav").css('border-color', '#ffffff');
        $(".navbar-toggler").css('border', '2px solid #ffffff');
        $(".navbar-toggler").css('color', '#ffffff');
        $(".navbar-toggler-icon").css('color', '#ffffff');
        $("#whiteLogo").show();
        $("#blackLogo").hide();

        if (target.length){
            $(document).scroll(function() {
                scroll_start = $(document).scrollTop();
                if(scroll_start > offset.top) {
                    $("#navbar").css('background-color', '#ffffff');
                    $(".nav-item").css('color', '#002424');
                    $(".active-nav").css('color', '#002424');
                    $(".active-nav").css('border-color', '#002424');
                    $("#navbar").addClass('navbar-box-shadow');
                    $("#logo").attr('src', '/img/GAIMS-Logo-Green.png');
                    $(".navbar-toggler").css('border', '2px solid #002424');
                    $(".navbar-toggler").css('color', '#002424');
                    $(".navbar-toggler-icon").css('color', '#002424');
                } else {
                    if($(".navbar-collapse").hasClass("show") == true){
                        $("#navbar").css('background-color', '#ffffff');
                        $(".nav-item").css('color', '#002424');
                        $(".navbar-toggler").css('border', '2px solid #002424');
                        $(".navbar-toggler").css('color', '#002424');
                        $(".navbar-toggler-icon").css('color', '#002424');
                    }else {
                        $("#navbar").css('background-color', 'transparent');
                        $(".nav-item").css('color', '#ffffff');
                        $(".active-nav").css('color', '#ffffff');
                        $(".active-nav").css('border-color', '#ffffff');
                        $("#navbar").removeClass('navbar-box-shadow');
                        $("#logo").attr('src', '/img/GAIMS-Logo-White.png');
                        $(".navbar-toggler").css('border', '2px solid #ffffff');
                        $(".navbar-toggler").css('color', '#ffffff');
                        $(".navbar-toggler-icon").css('color', '#ffffff');
                    }
                }
            });
        }
    }
});

