//   Declare basemaps collection
var basemaps = {
    Street: L.tileLayer(
        "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}", {
            maxZoom: 18,
            id: "mapbox/streets-v11",
            tileSize: 512,
            zoomOffset: -1,
            pane: "basemapPane",
            accessToken: "pk.eyJ1IjoibWFydmlubW9oZW51IiwiYSI6ImNrZXNvaDlsNDA4MXEzM3A3Yzd0MWRmenoifQ.-InEJi944UzkAb4HIDTtLA",
            attribution: "Developed by <a href=\"https://bigdataghana.com/\">BigData Ghana Ltd.</a> | &copy; <a href=\"https://www.mapbox.com/\">Mapbox</a>"
        }
    ).setZIndex(0),
    Satellite: L.tileLayer(
        "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}", {
            maxZoom: 18,
            id: "mapbox/satellite-v9",
            tileSize: 512,
            zoomOffset: -1,
            pane: "basemapPane",
            accessToken: "pk.eyJ1IjoibWFydmlubW9oZW51IiwiYSI6ImNrZXNvaDlsNDA4MXEzM3A3Yzd0MWRmenoifQ.-InEJi944UzkAb4HIDTtLA",
            attribution: "Developed by <a href=\"https://bigdataghana.com/\">BigData Ghana Ltd.</a> | &copy; <a href=\"https://www.mapbox.com/\">Mapbox</a>"
        }
    ).setZIndex(0),
    OpenStreetMap: L.tileLayer(
        'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            pane: "basemapPane",
            attribution: 'Developed by <a href=\"https://bigdataghana.com/\">BigData Ghana Ltd.</a> | &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }
    ).setZIndex(0),
};