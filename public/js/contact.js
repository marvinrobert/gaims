$(document).ready(function() {
    $('#suggestDatasetFormContainer').hide();

    $('#suggestDatasetFormBtn').click(function () {
        $('#sendMessageFormContainer').hide();
        $('#suggestDatasetFormContainer').show();

        $('#suggestDatasetFormBtn').addClass('form-btn-active');
        $('#sendMesssageFormBtn').removeClass('form-btn-active');
    });

    $('#sendMesssageFormBtn').click(function () {
        $('#suggestDatasetFormContainer').hide();
        $('#sendMessageFormContainer').show();

        $('#suggestDatasetFormBtn').removeClass('form-btn-active');
        $('#sendMesssageFormBtn').addClass('form-btn-active');
    });
});