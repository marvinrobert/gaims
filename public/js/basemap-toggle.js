// Basemap Toggle
$('#street-basemap-toggle').click(function () {
    $("#street-basemap-toggle").addClass("basemap-images-active");
    $("#street-basemap-toggle").removeClass("basemap-images");
    $("#satellite-basemap-toggle").addClass("basemap-images");
    $("#satellite-basemap-toggle").removeClass("basemap-images-active");
    $("#osm-basemap-toggle").addClass("basemap-images");
    $("#osm-basemap-toggle").removeClass("basemap-images-active");
    basemaps.Satellite.removeFrom(map);
    basemaps.OpenStreetMap.removeFrom(map);
    basemaps.Street.addTo(map);
});

$('#satellite-basemap-toggle').click(function () {
    $("#street-basemap-toggle").addClass("basemap-images");
    $("#street-basemap-toggle").removeClass("basemap-images-active");
    $("#satellite-basemap-toggle").addClass("basemap-images-active");
    $("#satellite-basemap-toggle").removeClass("basemap-images");
    $("#osm-basemap-toggle").addClass("basemap-images");
    $("#osm-basemap-toggle").removeClass("basemap-images-active");
    basemaps.Satellite.addTo(map);
    basemaps.Street.removeFrom(map);
    basemaps.OpenStreetMap.removeFrom(map);
});

$('#osm-basemap-toggle').click(function () {
    $("#street-basemap-toggle").addClass("basemap-images");
    $("#street-basemap-toggle").removeClass("basemap-images-active");
    $("#satellite-basemap-toggle").addClass("basemap-images");
    $("#satellite-basemap-toggle").removeClass("basemap-images-active");
    $("#osm-basemap-toggle").addClass("basemap-images-active");
    $("#osm-basemap-toggle").removeClass("basemap-images");

    basemaps.OpenStreetMap.addTo(map);
    basemaps.Street.removeFrom(map);
    basemaps.Satellite.removeFrom(map);
});