$(document).ready(function() {
    $("#submitRequestAccessAPI").hide();
    $(".radio-input").click(function() {
        $(".radio-input").prop('checked', false);
        $(this).prop('checked', true);
        $("#requestFormat").val($(this).val());
        if ($(this).val() == "CSV") {
            $("#submitRequestAccessAPI").hide();
            $("#submitRequestAccessCSV").show();
        } else {
            $("#submitRequestAccessCSV").hide();
            $("#submitRequestAccessAPI").show();
        }
    });

    var section = {
        select: "Select Section",
        agronomy: 'Agronomy & Agric Technology',
        policy: 'Policy & Administration',
        socio: 'Socio-Economic',
        nature: 'Natural Resources & Environment'
    }

    var category = {
        agronomy: {
            select: "Select Category",
            production: "Production Advice",
            pest: "Pest & Disease Management",
        },
        policy: {
            select: "Select Category",
            govtAgric: "Government, Agric Laws & Regulations",
            govtFinanceData: "Government Finance Data",
            officialRecords: "Official Records",
            ruralDev: "Rural Dev. Project Data",
        },
        socio: {
            select: "Select Category",
            landUse: "Land Use & Productivity",
            valueChain: "Value Chain",
            officialRecords: "Official Records",
            marketData: "Market Data",
        },
        nature: {
            select: "Select Category",
            meteorological: "Meteorological Data",
            elevation: "Elevation Data",
            hydrological: "Hydrological Data",
            soil: "Soil Data",
            vegetation: "Vegetation Data",
        }
    }

    var data = {
        agronomy: {
            production: {
                select: "Select Data",
                agroWarehouses: "Agro Warehouses",
                farms: "Farms",
                // agricMech: "Agric Mechanization",
                // tractors: "Tractors",
                // fertilizer: "Fertilizer Recommendations",
                // intercropping: "Intercropping",
                // relaycropping: "Relay Cropping",
                // rotations: "Rotations",
                // practice: "Practice Recommendations",
                // cropcalendars: "Crop Calendars",
                // data: "Data on Cultivation",
                // land: "Land Races and Farmer Varieties",
            },
            pest: {
                nodata: "No Data",
                // select: "Select Data",
                // occurence: "Occurences of Pest & Diseases",
                // treatment: "Treatment Recommendations",
                // recommended: "Recommended Pesticides",
                // agrochemical: "Agro Chemical Shops",
            },
        },
        policy: {
            govtAgric: {
                nodata: "No Data",
                // select: "Select Data",
                // subsidy: "Subsisdy Schemes",
                // animalHealth: "Animal Health & Welfare Regulations",
                // importExport: "Import | Export Regulations",
                // environmentalRegulations: "Environmental Regulations",
                // sanitaryRegulations: "Sanitary Regulations",
            },
            govtFinanceData: {
                nodata: "No Data",
                // select: "Select Data",
                // penalties: "Penalties Given to Agric Actors",
                // agricTax: "Agriculture Related Tax Income",
                // agricSubsidy: "Agriculture Subsidy Expenditure",
                // research: "Research & Education Investment",
            },
            officialRecords: {
                nodata: "No Data",
                // select: "Select Data",
                // permittedPesticides: "Permitted Pesticides",
                // importExport: "Import | Export Tariffs",
                // safetyInspection: "Safety Inspection Results",
                // approvedPermits: "Approved Permits",
                // licensedOrganizations: "Licensed Organizations",
                // landRegistration: "Land Registration",
            },
            ruralDev: {
                nodata: "No Data",
                // select: "Select Data",
                // projectDocuments: "Project Documents",
                // projectBaseline: "Project Baseline & Survey Data",
                // safetyInspection: "Safety Inspection Results",
                // projectOutput: "Project Output",
                // outcomeImpact: "Outcome & Impact",
                // generalInfo: "General Information",
            },
        },
        socio: {
            landUse: {
                nodata: "No Data",
                // select: "Select Data",
                // biomas: "Biomas",
                // animalHealth: "Animal Health & Welfare",
                // cropYield: "Crop Yield",
                // cultivatedCrops: "Cultivated Crops & Livestock",
                // landUse: "Land Use",
            },
            valueChain: {
                nodata: "No Data",
                // select: "Select Data",
                // productData: "Product Data",
                // companyProfiles: "Company Profiles",
                // foodSafety: "Food Safety Inspection Results",
            },
            officialRecords: {
                nodata: "No Data",
                // select: "Select Data",
                // internetCoverage: "Internet Coverage",
                // waterways: "Waterways",
                // roadManagement: "Road Management Schedules",
                // mobileTelephone: "Mobile Telephone Coverage",
                // roadNetworks: "Road Networks",
            },
            marketData: {
                nodata: "No Data",
                // select: "Select Data",
                // importExport: "Import | Export Volume",
                // marketActions: "Market & Actions",
                // globalFood: "Global Food Prices",
                // locationMarkets: "Location of Markets",
                // standards: "Standards",
                // grades: "Grades",
                // labeling: "Labeling",
                // marketManagement: "Market Management & Rules",
            },
        },
        nature: {
            meteorological: {
                nodata: "No Data",
                // select: "Select Data",
                // weatherStations: "Weather Stations",
                // climateChange: "Climate Change Predictions",
                // climateZones: "Climate Zones",
                // observationArchives: "Observation Archives",
                // realTime: "Real Time Observation",
                // shortTerm: "Short Term Weather Forecast",
            },
            elevation: {
                // nodata: "No Data",
                select: "Select Data",
                digitalElevationModelColors: "Digital Elevation Model (Colored)",
                digitalElevationModelGreyscale: "Digital Elevation Model (Greyscale)",
                // highPoints: "High Points",
                // slopeData: "Slope Data",
                // aspectData: "Aspect Data",
            },
            hydrological: {
                nodata: "No Data",
                // select: "Select Data",
                // waterManagement: "Water Management",
                // waterTables: "Water Tables",
                // waterQuality: "Water Quality",
                // realTime: "Real Time Water Levels",
                // historicalRecords: "Historical Records on Flooding",
                // floodZones: "Flood Zones",
                // waterBalance: "Water Balance",
                // location: "Location of Water Sources",
            },
            soil: {
                nodata: "No Data",
                // select: "Select Data",
                // soilClasses: "Soil Classes",
                // soilSamples: "Soil Samples",
                // soilMaps: "Soil Maps",
            },
            vegetation: {
                select: "Select Data",
                ndvi: "NDVI",
            },
        },
    }

    //populate section
    const sectionKeys = Object.keys(section);
    const keys = sectionKeys.filter(e => e !== "select");
    const selectOptions = sectionKeys.map(s => "<option value='" + section[s] + "'>" + section[s] + "</option>");
    $("#requestSection").append(selectOptions);

    $(".modal-click").click(function() {
        map.dragging.disable();
        map.touchZoom.disable();
        map.doubleClickZoom.disable();

        //get selected section
        const currentSection = keys.filter(key => $(this).hasClass(key))[0];
        $("#requestSection").val(section[currentSection]);

        //populate categories
        const currentCategories = category[currentSection];
        const categoryKeys = Object.keys(currentCategories)
        const categoryOptions = categoryKeys.map(ck => "<option value='" + currentCategories[ck] + "'>" + currentCategories[ck] + "</option>");
        $("#requestCategory").html("");
        $("#requestCategory").append(categoryOptions);

        //choose selected category
        const currentCategory = categoryKeys.filter(key => $(this).hasClass(key))[0];
        $("#requestCategory").val(currentCategories[currentCategory]);

        //populate data
        const currentData = data[currentSection][currentCategory];
        const dataKeys = Object.keys(currentData)
        const dataOptions = dataKeys.map(d => "<option value='" + currentData[d] + "'>" + currentData[d] + "</option>");
        $("#requestData").html("");
        $("#requestData").append(dataOptions);
    });

    $(".close").click(function() {
        map.dragging.enable();
        map.touchZoom.enable();
        map.doubleClickZoom.enable();

        $("#requestName").val("");
        $("#requestEmail").val("");
        $("#requestSection").val("Select Section");
        $("#requestCategory").val("Select Category");
        $("#requestData").val("Select Data");
    });

    $("#requestSection").on('change', function() {
        const sectionValues = Object.values(section);
        const values = sectionValues.filter(f => f !== "Select Section");

        //get selected section
        const currentSectionValue = $(this).val();
        const currentSection = values.filter(value => value == currentSectionValue)[0];
        const currentSectionKey = Object.keys(section).find(key => section[key] === currentSection);

        //populate categories
        const currentCategory = category[currentSectionKey];
        const categoryKeys = Object.keys(currentCategory)
        const categoryOptions = categoryKeys.map(ck => "<option value='" + currentCategory[ck] + "'>" + currentCategory[ck] + "</option>");
        $("#requestCategory").html("");
        $("#requestData").html("");
        $("#requestCategory").append(categoryOptions);
    });

    $("#requestCategory").on('change', function() {
        const sectionValues = Object.values(section);
        const filteredSectionValues = sectionValues.filter(f => f !== "Select Section");

        //get selected section
        const currentSectionValue = $("#requestSection").val();
        const currentSection = filteredSectionValues.filter(value => value == currentSectionValue)[0];
        const currentSectionKey = Object.keys(section).find(key => section[key] === currentSection);


        const categoryValues = Object.values(category[currentSectionKey]);
        const filteredCategoryValues = categoryValues.filter(f => f !== "Select Category");

        //get selected category
        const currentCategoryValue = $(this).val();
        const currentCategory = filteredCategoryValues.filter(value => value == currentCategoryValue)[0];
        const currentCategoryKey = Object.keys(category[currentSectionKey]).find(key => category[currentSectionKey][key] === currentCategory);

        //populate data
        const currentData = data[currentSectionKey][currentCategoryKey];
        const dataKeys = Object.keys(currentData);
        const dataOptions = dataKeys.map(ck => "<option value='" + currentData[ck] + "'>" + currentData[ck] + "</option>");
        $("#requestData").html("");
        $("#requestData").append(dataOptions);
    });
});