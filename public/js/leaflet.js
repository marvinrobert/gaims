// GAIMS WMS
// Map

if (window.location.href.indexOf("/open-data") > -1) {
    var map = L.map('mapContainer', {zoomControl: false}).setView([7.9527706, -1.0307118], 7);

    // Create Panes
    map.createPane('basemapPane').style.zIndex= 400;
    map.createPane('ndviPane').style.zIndex= 450;
    map.createPane('demPane').style.zIndex= 450;
    map.createPane('regionalPane').style.zIndex= 500;
    map.createPane('districtPane').style.zIndex= 500;
    map.createPane('constituencyPane').style.zIndex= 500;
    map.createPane('farmsPane').style.zIndex= 550;
    map.createPane('agroWarehousesPane').style.zIndex= 600;

    //Scale
    L.control.scale({
        position: 'topright',
    }).addTo(map);

// Add zoom control
    var zoomHome = L.Control.zoomHome({
        position: "topright",
    });
    zoomHome.addTo(map);

// Add Fullscreen control
    map.addControl(
        new L.Control.Fullscreen({
            position: "topright",
        })
    );

// Set initial basemap
    basemaps.Street.addTo(map);

// SIDEBAR
    var sidebar = L.control.sidebar({
        autopan: true,       // whether to maintain the centered map point when opening the sidebar
        closeButton: true,    // whether t add a close button to the panes
        container: 'sidebar', // the DOM container or #ID of a predefined sidebar container that should be used
        position: 'left',     // left or right
    }).addTo(map);

    // disable drag and zoom on modal show
    // $(".modalClick").click(function (){
    //     map.dragging.disable();
    //     map.touchZoom.disable();
    //     map.doubleClickZoom.disable();
    // });

    // enable drag and zoom on modal show
    // $(".close").click(function (){
    //     map.dragging.enable();
    //     map.touchZoom.enable();
    //     map.doubleClickZoom.enable();
    //
    //     $("#requestName").val("");
    //     $("#requestEmail").val("");
    //     $("#requestSection").val("Select Section");
    //     $("#requestCategory").val("Select Category");
    //     $("#requestData").val("Select Data");
    // });
}
