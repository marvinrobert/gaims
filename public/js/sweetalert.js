$(document).ready(function (){
    $("#sendingRequestOverlay").hide();
    $("#submitRequestAccessAPI").click(function (){
        $(document).ajaxSend(function() {
            $("#sendingRequestOverlay").fadeIn(300);
        });
        // Form validation
        if($("#requestCategory").val() == "" || $("#requestCategory").val() == "Select Category"){
            // alert("Please choose category!");
            swal({
                title: "Error",
                text: "Please choose category!",
                icon: "error",
                button: "Okay",
            });
        }
        else if($("#requestSection").val() == "" || $("#requestSection").val() == "Select Section"){
            // alert("Please choose section!");
            swal({
                title: "Error",
                text: "Please choose section!",
                icon: "error",
                button: "Okay",
            });
        }
        else if($("#requestData").val() == "" || $("#requestData").val() == "Select Data"){
            // alert("Please choose data!");
            swal({
                title: "Error",
                text: "Please choose data!",
                icon: "error",
                button: "Okay",
            });
        }
        else if($("#requestName").val() == ""){
            // alert("Please enter your name!");
            swal({
                title: "Error",
                text: "Please enter your name!",
                icon: "error",
                button: "Okay",
            });
        }
        else if($("#requestEmail").val() == "" || validateEmail($("#requestEmail").val()) == false){
            // alert("Please enter a valid email!");
            swal({
                title: "Error",
                text: "Please enter a valid email!",
                icon: "error",
                button: "Okay",
            });
        }
        else if($("#requestData").val() == "No Data"){
            swal({
                title: "Data not yet available!",
                text: "Kindly check back later. Thank you!",
                icon: "error",
                button: "Okay",
            });
        }
        else {
            $.ajax({
                url: "/form/requestaccess",
                method: "post",
                data: {
                    requestName: $("#requestName").val(),
                    requestEmail: $("#requestEmail").val(),
                    requestCategory: $("#requestSection").val(),
                    requestCategory: $("#requestCategory").val(),
                    requestData: $("#requestData").val(),
                    requestFormat: $("#requestFormat").val(),
                }
            }).done(d => {
                // setTimeout(function(){
                    $("#sendingRequestOverlay").fadeOut(300);
                // },500);
                swal({
                    title: "Success",
                    text: "Request Sent Successfully",
                    icon: "success",
                    button: "Okay",
                });
            });
        }
    });

    function validateEmail(mail)
    {
        if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(mail))
        {
            return (true)
        }
        return (false)
    }
})