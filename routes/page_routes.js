const express = require('express');
const router = express.Router();

// Index route
router.get('/', (req, res) => {
    res.render('home');
});

// Maps route
router.get('/maps', (req, res) => {
    res.render('maps');
});

// Events route
router.get('/events', (req, res) => {
    res.render('Events');
});

// News route
router.get('/news', (req, res) => {
    res.render('News');
});

// More route
router.get('/more', (req, res) => {
    res.send('More');
});

// Login route
router.get('/login', (req, res) => {
    res.send('Login');
});

module.export = router;